package Cars;

public class Nissan extends Cars {
	
	
	public Nissan(int power, int weight) {
		super(power, weight);
		
	}

	@Override
	public void printEnginePower() {
		System.out.println("Nissan power is:"+ power);
	}

	@Override
	public int getWeight() {
		return weight;
	}
	public String toString() {
		return "Nissan with power "+power+" and weigth "+weight;
	}

}
