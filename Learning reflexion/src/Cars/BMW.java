package Cars;

public class BMW extends Cars {

	public BMW(int power, int weight) {
		super(power, weight);
		
	}

	@Override
	public void printEnginePower() {
		System.out.println("BMW power is:"+power);
	}

	@Override
	public int getWeight() {
		return weight;
	}
	public String toString() {
		return "BMW with power "+power+" and weigth "+weight;
	}

}
