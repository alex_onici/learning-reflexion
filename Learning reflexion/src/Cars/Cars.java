package Cars;

public abstract class Cars {
	protected int weight;
	protected int power;
	abstract public void printEnginePower();
	abstract public  int getWeight();
	public Cars(int power, int weight) {
		this.power = power;
		this.weight = weight;
	}


}
