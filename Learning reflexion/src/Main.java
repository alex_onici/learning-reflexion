import java.lang.reflect.Field;
import java.lang.reflect.Method;

import Cars.BMW;
import Cars.Cars;
import Cars.Nissan;
import Cars.Toyota;

public class Main {

	public static void main(String [] args) {
		
		Cars car  = new BMW(520, 2000);
		Cars car2 = new Nissan(320, 1500);
		Cars car3 = new Toyota(350, 1800);
		Method [] methods = Cars.class.getMethods();
		Field [] fields = Cars.class.getFields();
		for(Method cur:methods) {
			System.out.println(cur.getName());
		}
		for(Field cur:fields) {
			System.out.println(cur.getName());
		}
		Parking park = new Parking();
		park.addCars(car);
		park.addCars(car2);
		park.addCars(car3);
		park.printCars();
	}
}
