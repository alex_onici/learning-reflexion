package Cars;

public class Toyota extends Cars {

	public Toyota(int power, int weight) {
		super(power, weight);
	}

	@Override
	public void printEnginePower() {
		System.out.println("Toyota power is:"+power);
	}

	@Override
	public int getWeight() {
		return weight;
	}
	public String toString() {
		return "Toyota with power "+power+" and weigth "+weight;
	}

}
