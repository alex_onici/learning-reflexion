import java.util.ArrayList;

import Cars.Cars;

public class Parking {
	ArrayList<Cars> list = new ArrayList();
	public void addCars(Cars car) {
		list.add(car);
	}
	public void removeCars(Cars car) {
		list.remove(car);
	}
	public void printCars() {
		list.forEach(p->System.out.println(p.toString()));
	}

}
